export BUILD_TOPDIR=$(PWD)
export STAGING_DIR=$(BUILD_TOPDIR)/tmp
export TOPDIR=$(PWD)
export UBOOTDIR=$(TOPDIR)/u-boot

### Toolchain config ###
#buildroot
#CONFIG_TOOLCHAIN_PREFIX=/opt/build/toolchain-mipsbe-4.7.3/bin/mips-linux-

CONFIG_TOOLCHAIN_PREFIX=mips-buildroot-linux-uclibc-
export PATH:=$(PWD)/buildroot/output/host/usr/bin/:$(PATH)

########################

export CROSS_COMPILE=$(CONFIG_TOOLCHAIN_PREFIX)
export MAKECMD=make ARCH=mips

export UBOOT_GCC_4_3_3_EXTRA_CFLAGS=-fPIC
export BUILD_TYPE=squashfs

export COMPRESSED_UBOOT=0
export CARABOOT_RELEASE=$(shell git describe --dirty --always --match="ar9331-v*")

IMAGEPATH=$(BUILD_TOPDIR)/bin
UBOOT_BINARY=u-boot.bin
BOARD_TYPE:=

compile:
	cd $(UBOOTDIR) && $(MAKECMD) distclean
	cd $(UBOOTDIR) && $(MAKECMD) $(BOARD_TYPE)_config
	cd $(UBOOTDIR) && $(MAKECMD) all
	@echo Copy binaries to $(IMAGEPATH)/$(UBOOTFILE)
	mkdir -p $(IMAGEPATH)
	cp -f $(UBOOTDIR)/$(UBOOT_BINARY) $(IMAGEPATH)/$(UBOOTFILE)

	@echo Done

carambola2: BOARD_TYPE:= carambola2
carambola2: UBOOTFILE=$(BOARD_TYPE)_u-boot.bin
carambola2: compile

ap1: BOARD_TYPE:= ap1
ap1: UBOOTFILE=$(BOARD_TYPE)_u-boot.bin
ap1: compile

ap2: BOARD_TYPE:= ap2
ap2: UBOOTFILE=$(BOARD_TYPE)_u-boot.bin
ap2: compile

jb02v3-skw: BOARD_TYPE:= jb02v3-skw
jb02v3-skw: UBOOTFILE=$(BOARD_TYPE)_u-boot.bin
jb02v3-skw: compile

jb02v2-cb2: BOARD_TYPE:= jb02v2-cb2
jb02v2-cb2: UBOOTFILE=$(BOARD_TYPE)_u-boot.bin
jb02v2-cb2: compile

all: carambola2 ap1 ap2 jb02v3-skw jb02v2-cb2

clean:
	cd $(UBOOTDIR) && $(MAKECMD) distclean
