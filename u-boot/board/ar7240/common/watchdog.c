#include <common.h>
#include <asm/addrspace.h>
#include <asm/types.h>
#include "ar7240_soc.h"

void hw_watchdog_reset(void)
{
  /* around 15 seconds */
  ar7240_reg_wr(AR7240_WATCHDOG_TMR, 0xfffffffful);

  /* do a full chip reset */
  ar7240_reg_wr(AR7240_WATCHDOG_TMR_CONTROL, 0x3);
}
