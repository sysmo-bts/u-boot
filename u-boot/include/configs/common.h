
#define CFG_FLASH_WORD_SIZE     unsigned short 

/* 
 * We boot from this flash
 */
#define CFG_FLASH_BASE		    0x9f000000
#define CFG_MAX_FLASH_SECT	    4096
#define CFG_MAX_FLASH_BANKS     1
#define CFG_MAX_FLASH_SECT      256

#define CFG_CMD_SETMAC

#ifdef COMPRESSED_UBOOT
#define BOOTSTRAP_TEXT_BASE         CFG_FLASH_BASE
#define BOOTSTRAP_CFG_MONITOR_BASE  BOOTSTRAP_TEXT_BASE
#endif

/* 
 * The following #defines are needed to get flash environment right 
 */
#define	CFG_MONITOR_BASE	TEXT_BASE
#define	CFG_MONITOR_LEN		(192 << 10)

#undef CONFIG_BOOTARGS
/* XXX - putting rootfs in last partition results in jffs errors */

/* default mtd partition table */
#undef MTDPARTS_DEFAULT

#define	CONFIG_BOOTARGS     ""
#define MTDPARTS_DEFAULT    ""

/* 
 * timeout values are in ticks 
 */
#define CFG_FLASH_ERASE_TOUT	(2 * CFG_HZ) /* Timeout for Flash Erase */
#define CFG_FLASH_WRITE_TOUT	(2 * CFG_HZ) /* Timeout for Flash Write */

/*
 * Cache lock for stack
 */
#define CFG_INIT_SP_OFFSET	0x1000

#ifndef COMPRESSED_UBOOT
#define	CFG_ENV_IS_IN_FLASH    1
#undef CFG_ENV_IS_NOWHERE  
#else
#undef  CFG_ENV_IS_IN_FLASH
#define CFG_ENV_IS_NOWHERE  1
#endif /* #ifndef COMPRESSED_UBOOT */

/* Address and size of Primary Environment Sector	*/
#define CFG_ENV_ADDR		0x9f040000
#define CFG_ENV_SIZE		0x10000

/* check if boot cycle threshold reached : if not try to boot linux if this fails run recovery */
#define CONFIG_BOOTCOMMAND "inccycle ; if checkcycle ; then bootm 0x9f050000 ; run_recovery ; else run_recovery ; fi ; run_recovery"
#define CFG_BOOTM_LEN	(16 << 20) /* 16 MB */

#define CONFIG_NET_MULTI

/* dummy addr - it will be read from art partition. See BOARDCAL */
#define CONFIG_ETHADDR 0x00:0xaa:0xbb:0xcc:0xdd:0xee

#define CONFIG_MEMSIZE_IN_BYTES

#ifdef COMPRESSED_UBOOT
#define ATH_NO_PCI_INIT
#endif

/*-----------------------------------------------------------------------
 * Cache Configuration
 */
#ifndef COMPRESSED_UBOOT
#define CONFIG_COMMANDS	(( CONFIG_CMD_DFL | CFG_CMD_DHCP | CFG_CMD_ELF | CFG_CMD_PCI |	\
	CFG_CMD_MII | CFG_CMD_PING | CFG_CMD_NET | CFG_CMD_ENV | \
	CFG_CMD_FLASH | CFG_CMD_LOADS | CFG_CMD_RUN | CFG_CMD_LOADB | CFG_CMD_ELF | CFG_CMD_ETHREG | CFG_CMD_FAT | CFG_CMD_SAVES ))
#elif defined(VXWORKS_UBOOT)
#define CONFIG_COMMANDS	(( CONFIG_CMD_DFL | CFG_CMD_PING | CFG_CMD_NET | CFG_CMD_MII | CFG_CMD_ELF))
#else
#define CONFIG_COMMANDS	(( CONFIG_CMD_DFL | CFG_CMD_PING | CFG_CMD_NET | CFG_CMD_MII))
#endif /* #ifndef COMPRESSED_UBOOT */

#undef  DEBUG
#define CFG_CONSOLE_INFO_QUIET		/* don't print console @ startup*/
#define CONFIG_SHOW_BOOT_PROGRESS	/* use LEDs to show boot status*/
#define CONFIG_SHOW_ACTIVITY
#define CFG_HUSH_PARSER
#define CFG_PROMPT_HUSH_PS2 "hush>"

/* platform specific */
#define ARCH_DMA_MINALIGN 		4*1024 // 4kb in datasheet
#define CONFIG_SYS_HZ      		1000 
#define CONFIG_SYS_MAXARGS 		16
#define HAVE_BLOCK_DEVICE
#define CONFIG_PARTITIONS
#define CONFIG_DOS_PARTITION
#define CONFIG_FS_FAT
#define CONFIG_SUPPORT_VFAT
#define CONFIG_SYS_LOAD_ADDR 		0x82000000
#define CONFIG_NEEDS_MANUAL_RELOC

#define CONFIG_AUTOBOOT_KEYED
#define CONFIG_AUTOBOOT_STOP_STR 		"ap-stop"
#define CONFIG_BOOTDELAY       3

#undef CFG_BAUDRATE_TABLE
#define CFG_BAUDRATE_TABLE { 300, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 56000, 57600, \
			115200, 128000, 230400, 250000, 256000, 460800, 500000, 576000, 921600, \
			1000000, 1152000, 1500000, 2000000, 3000000}

/* calibration */
#define ATHEROS_PRODUCT_ID             138
#define CAL_SECTOR                     (CFG_MAX_FLASH_SECT - 1)

/* For Kite, only PCI-e interface is valid */
#define AR7240_ART_PCICFG_OFFSET        3

