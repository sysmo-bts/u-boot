/*
 * This file contains the configuration parameters for the dbau1x00 board.
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include <configs/ar7240.h>
#include <config.h>

#define CONFIG_AR7240 1
#define CONFIG_MACH_HORNET 1
#define CONFIG_HORNET_1_1_WAR 1

/* enable watchdog */
#define CONFIG_HW_WATCHDOG

#define CFG_CMD_BOOTCYCLE

/*-----------------------------------------------------------------------
 * FLASH and environment organization
 *-----------------------------------------------------------------------
 */

#define DEFAULT_FLASH_SIZE_IN_MB 16

#define BOARDCAL                        0x9fff0000

#if (CFG_MAX_FLASH_SECT * CFG_FLASH_SECTOR_SIZE) != CFG_FLASH_SIZE
#	error "Invalid flash configuration"
#endif

/* pll and ddr configuration */
#define CONFIG_40MHZ_XTAL_SUPPORT 1
#define NEW_DDR_TAP_CAL 1

#undef CFG_PLL_FREQ
#define CFG_PLL_FREQ	CFG_PLL_400_400_200

#include <configs/ar7240_freq.h>

#define CFG_ATHRS26_PHY  1

#define CONFIG_IPADDR   192.168.2.100
#define CONFIG_SERVERIP 192.168.2.254
#define CFG_FAULT_ECHO_LINK_DOWN    1

#define RECOVERY_FILENAME_STRING "openwrt-ar71xx-generic-sysmocom-sob-jb02-initramfs-uImage.bin"

#define CFG_PHY_ADDR 0 

#define CFG_AG7240_NMACS 2

#define CFG_GMII     0
#define CFG_MII0_RMII             1
#define CFG_AG7100_GE0_RMII             1

#include <configs/common.h>

#include <cmd_confdefs.h>

#endif	/* __CONFIG_H */
